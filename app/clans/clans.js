'use strict';

var app = angular.module('dragon.clans', ['ngRoute']);

app.config([
  '$routeProvider', function($routeProvider) {
    $routeProvider.when('/clans', {
      templateUrl: '/clans/clans.html',
      controller: 'ClanCtrl'
    });
  }
])
  
  .controller('ClanCtrl', function($scope, $firebaseArray, Auth) {
    var ref = firebase.database().ref().child("clans");
    // create a synchronized array
    $scope.clans = $firebaseArray(ref);
    
    // Auth - Log in
    $scope.login = function() {
      // reset 
      $scope.user = null;
      $scope.error = null;
      // ask user to log in
      Auth.$signInWithPopup("google")
      // save the auth state
        .then(function(firebaseUser) {
          $scope.user = firebaseUser;
        })
        .catch(function(error) {
          $scope.error = error;
        });
    };
    
    // Auth - Sign Out
    $scope.signOut = function() {
      Auth.$signOut();
    };
    
    // any time auth status updates, add the user data to scope
    Auth.$onAuthStateChanged(function(firebaseUser) {
      $scope.user = firebaseUser;
    });
  });
