'use strict';

var app = angular.module('dragon.chats', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: '/chats/chats.html',
    controller: 'ChatsCtrl'
  });
}])

.controller('ChatsCtrl', function($scope, $firebaseArray) {
  var ref = firebase.database().ref().child("chats");
  // create a synchronized array
  $scope.chats = $firebaseArray(ref);
});
