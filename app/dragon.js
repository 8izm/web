'use strict';

var app = angular.module('dragon', [
  'ngRoute',
  'dragon.clans',
  'dragon.detail',
  'dragon.add',
  'dragon.add.faq',
  'dragon.chats',
  'dragon.faq',
  'firebase'
])
  
  .config([
    '$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
      $locationProvider.html5Mode(true).hashPrefix('!');
      
      $routeProvider.otherwise({redirectTo: '/'});
    }
  ])
  
  .factory("Auth", [
    "$firebaseAuth",
    function($firebaseAuth) {
      return $firebaseAuth();
    }
  ])

  .run([
    '$location', '$rootScope', function($location, $rootScope) {
      $rootScope.$on("$routeChangeError", function(event, next, previous, error) {
        if(error === "AUTH_REQUIRED") {
          $location.path("/clans");
        }
      });
      ga('send', 'pageview');
    }
  ]);